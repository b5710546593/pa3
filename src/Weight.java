/**
 * Enum that contain unit of Weight.
 * @author Voraton Lertrattanapaisal
 *
 */
public enum Weight implements Unit{
	KG("Kilogram",1),
	TON("Ton",0.001),
	KN("Kilonewton(Earth)",0.009807),
	C("Centner",0.01),
	N("Newton",9.807),
	CT("Carat",5000),
	G("Gram",1000),
	CG("Centigram",100000),
	MG("Milligram",1000000),
	UG("Microgram",1000000000),
	AMU("Atomic Mass Unit",6.022e+26),
	LT("Long Ton(UK)",0.0009842),
	ST("Shot Ton(US)",0.001102),
	KIP("Kilopound",0.002205),
	LH("Long Hunredweight(UK)",0.01968),
	SH("Short Hunredweight(US)",0.02205),
	S("Stone",0.1575),
	IB("Pound",2.205),
	OZ("Once",35.27),
	DR("Dram",564.4),
	GR("Grain",15430),
	IBT("Pound(Troy)",2.679),
	OZT("Ounce(Troy)",32.15),
	PWT("Pennyweight",643),
	CTT("Carat(Troy)",4878),
	M("Mite",308600),
	D("Doite",7408000),
	DA("Dram(Apothecaries)",257.2),
	SC("Scruple",771.6),
	PM("Planck Mass",45950000),
	KM("Kanme",0.2667),
	KIN("Kin",1.667),
	HK("Hyakume",2.667),
	MM("Monnme",266.7),
	FUN("Fun",2667),
	DAN("Dan",0.02),
	JIN("Jin",2),
	L("Liang",20),
	Q("Qian",200),
	FEN("Fen",2000),
	LI("Li",20000),
	HAO("Hao",200000),
	SI("Si",2000000),
	HU("Hu",20000000),
	HAP("Hap",0.0164),
	Chang("Chang",0.82),
	TL("Tamlueng",16.4),
	BT("Baht",66.67),
	BTG("Baht(Gold)",65.96),
	MY("Mayong",131.2),
	SL("Salueng",262.4),
	F("Fueng",524.8),
	SEEK("Seek",1050),
	SP("Siao / Pai" ,2099),
	ATH("Ath",4198),
	SLO("Solos",8397),
	BIA("Bia",419800),
	PL("Picul",0.01653),
	CAT("Catty",1.653),
	TAEL("Tael",26.46),
	MC("Mace",264.6),
	CD("Candareen",2646),
	TAELT("Tael(Troy)",26.72),
	MCT("Mace (Troy)",267.2),
	CDT("Candareen (Troy)",2672);
	
	public final String name;
	public final double value;
	/**
	 * To initialize the Unit of Weight.
	 * @param name is name of unit.
	 * @param value is base value of unit.
	 */
	private Weight(String name,double value){
		this.name=name;
		this.value=value;
	}
	/**
	 * To get base value of unit.
	 */
	public double getValue(){
		return value;
	}
	/**
	 * To convert amount to other unit.
	 * @param amt is amount to convert.
	 * @param unit is other unit that you want to convert to.
	 */
	public double convertTo(double amt,Unit unit){
		return (amt*this.getValue())/(unit.getValue());
	}
	/**
	 * To return unit name.
	 */
	public String toString(){
		return name;
	}
}
