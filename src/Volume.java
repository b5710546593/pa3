/**
 * Enum that contain unit of Volume.
 * @author Voraton Lertrattanapaisal
 *
 */
public enum Volume implements Unit{
	M3("Cubic Meter",1),
	KM3("Cubic Kilometer",0.000000001),
	DM3("Cubic Decimeter",1000),
	CC("Cubic Centimeter",1000000),
	MM3("Cubic Millimeter",1000000000),
	HL("Hectoliter",10),
	DC("Decaliter",100),
	L("Liter",1000),
	DL("Deciliter",10000),
	CL("Centiliter",100000),
	ML("Milliliter",1000000),
	UL("Microliter",1000000000),
	B("Barrel",8.648),
	BU("Bushel",28.38),
	PK("Peck",113.5),
	GAL("Gallon",227),
	Q("Quart",908.1),
	PT("Pint",1816),
	AF("Acre Foot",0.0008107),
	BP("Barrel (Petroleum)",6.29),
	GILL("Gill",8454),
	OZ ("Ounce",33810),
	KWIAN("Kwian",0.5),
	SAT("Sat",40),
	TH("Thang",50),
	THN("Thanan",1000),
	FM("Fai Mue",8000),
	KM("Kam Mue",32000),
	YM("Yip Mue",128000);
	public final String name;
	public final double value;
	/**
	 * To initialize the Unit of Volume.
	 * @param name is name of unit.
	 * @param value is base value of unit.
	 */
	private Volume(String name,double value){
		this.name=name;
		this.value=value;
	}
	/**
	 * To get base value of unit.
	 */
	public double getValue(){
		return value;
	}
	/**
	 * To convert amount to other unit.
	 * @param amt is amount to convert.
	 * @param unit is other unit that you want to convert to.
	 */
	public double convertTo(double amt,Unit unit){
		return (amt*this.getValue())/(unit.getValue());
	}
	/**
	 * To return unit name.
	 */
	public String toString(){
		return name;
	}
}
