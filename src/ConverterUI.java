import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 * Distance Converter Program.
 * @author Voraton Lertrattanapaisal
 *
 */
public class ConverterUI extends JFrame implements Runnable{
	private UnitConverter uc;
	private UnitType ut;
	private JComboBox fromBox;
	private JComboBox toBox;
	private JTextField fromAmount;
	private JTextField toAmount;
	private JButton conBut;
	private JButton clearBut;
	/**
	 * Constructor for create new converter.
	 * @param uc is unit converter for convert thing in background.
	 */
	public ConverterUI( UnitConverter uc){
		super("Unit Converter");
		this.uc=uc;
		this.ut = UnitType.LengthUnit;
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		initComponent();
	}
	/**
	 * To initialize the component.
	 */
	private void initComponent(){
		Container content = getContentPane();
		content.setLayout(new FlowLayout());
		JMenuBar menubar = new JMenuBar();
		JMenu menu = new JMenu("File");
		JMenuItem LengthItem = new JMenuItem("Length");
		JMenuItem AreaItem = new JMenuItem("Area");
		JMenuItem WeightItem = new JMenuItem("Weight");
		JMenuItem VolumeItem = new JMenuItem("Volume");
		LengthItem.addActionListener(new MenuListener(UnitType.LengthUnit));
		AreaItem.addActionListener(new MenuListener(UnitType.AreaUnit));
		WeightItem.addActionListener(new MenuListener(UnitType.WeightUnit));
		VolumeItem.addActionListener(new MenuListener(UnitType.VolumeUnit));
		menubar.add(menu);
		menu.add(LengthItem);
		menu.add(AreaItem);
		menu.add(WeightItem);
		menu.add(VolumeItem);
		menu.add(new ExitAction());
		super.setJMenuBar(menubar);
		fromAmount = new JTextField(10);
		fromBox = new JComboBox(uc.getUnits(ut));
		toAmount = new JTextField(10);
		toBox = new JComboBox(uc.getUnits(ut));
		conBut = new JButton("Convert");
		clearBut = new JButton("Clear");
		conBut.addActionListener(new ConvertButtonListener(1));
		clearBut.addActionListener(new ClearButtonListener());
		fromAmount.addActionListener(new ConvertButtonListener(1));
		toAmount.addActionListener(new ConvertButtonListener(2));
		content.add(fromAmount);
		content.add(fromBox);
		content.add(toAmount);
		content.add(toBox);
		content.add(conBut);
		content.add(clearBut);
		pack();	
	}
	/**
	 * For run the windows of converter.
	 */
	public void run(){
		setVisible(true);
	}
	/**
	 * ActionListener of Convert Button for convert data.
	 * @author Voaton Lertrattanapaisal
	 *
	 */
	private class ConvertButtonListener implements ActionListener {
		private int order;
		public ConvertButtonListener(int order){
			this.order=order;
		}
		public void actionPerformed (ActionEvent e){
			try{
				if (order==1){
					double from = Double.parseDouble(fromAmount.getText().trim());
					if (from <0){
						throw new IllegalArgumentException();
					}
					else {
						toAmount.setText(String.format("%.5g",uc.convert(from, (Unit)fromBox.getSelectedItem(), (Unit)toBox.getSelectedItem())));
					}
				}
				if (order==2){
					double from = Double.parseDouble(toAmount.getText().trim());
					if (from <0){
						throw new IllegalArgumentException();
					}
					else {
						fromAmount.setText(String.format("%.5g",uc.convert(from, (Unit)toBox.getSelectedItem(), (Unit)fromBox.getSelectedItem())));
					}
				}
				fromAmount.setForeground(Color.BLACK);
				toAmount.setForeground(Color.BLACK);
			}
			catch(NumberFormatException er){
				if (order==1){
					JOptionPane.showMessageDialog( null,fromAmount.getText()+" is incorrect data!","Error!",JOptionPane.INFORMATION_MESSAGE);
					fromAmount.setForeground(Color.RED);
				}
				if (order==2){
					JOptionPane.showMessageDialog( null,toAmount.getText()+" is incorrect data!","Error!",JOptionPane.INFORMATION_MESSAGE);
					toAmount.setForeground(Color.RED);
				}
			}
			catch (IllegalArgumentException c){
				JOptionPane.showMessageDialog( null,"Data can't be negative!","Error!",JOptionPane.INFORMATION_MESSAGE);
				if (order==1){
					fromAmount.setForeground(Color.RED);
				}
				if (order==2){
					toAmount.setForeground(Color.RED);
				}
			}
			catch(Exception o){
				JOptionPane.showMessageDialog( null,"Error","Error!",JOptionPane.INFORMATION_MESSAGE);
				if (order==1){
					fromAmount.setForeground(Color.RED);
				}
				if (order==2){
					toAmount.setForeground(Color.RED);
				}
			}
		}
	}
	/**
	 * ActionListener for Clear Button for clear data.
	 * @author Voraton Lertrattanapaisal
	 *
	 */
	private class ClearButtonListener implements ActionListener{
		public void actionPerformed(ActionEvent e){
			fromAmount.setText("");
			toAmount.setText("");
		}
	}
	/**
	 * ActionListener for Menu
	 * @author Voraton Lertrattanapaisal
	 *
	 */
	private class MenuListener implements ActionListener{
		private UnitType unit;
		public MenuListener(UnitType unit){
			this.unit=unit;
		}
		public void actionPerformed(ActionEvent e){
			toBox.removeAllItems();
			fromBox.removeAllItems();
			ut = unit;
			for(Unit u : uc.getUnits(ut)) {
				   toBox.addItem(u);
			}
			for(Unit u : uc.getUnits(ut)) {
				   fromBox.addItem(u);
			}
			pack();
		}
	}
	/**
	 * AbstractAction for Exit Program
	 * @author Voraton Lertrattanapaisal
	 *
	 */
	private class ExitAction extends AbstractAction {
		public ExitAction( ) {
			super("Exit");
		}
		public void actionPerformed(ActionEvent evt) {
			System.exit(0);
		}
	}

}
