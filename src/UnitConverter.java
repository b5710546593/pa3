/**
 * Unit converter is unit converter and it can tell you all of the units.
 * @author Voraton Lertrattanapaisal
 *
 */
public class UnitConverter {
	/**
	 * For Convert From unit to other unit.
	 * @param amount is amount that want to convert.
	 * @param fromUnit is original unit of amount.
	 * @param toUnit is new unit of amount.
	 * @return amount in new unit.
	 */
	public double convert(double amount,Unit fromUnit,Unit toUnit){
		return fromUnit.convertTo(amount, toUnit);
	}
	/**
	 * For get all of unit that unit converter can convert.
	 * @return List of units.
	 */
	public Unit[] getUnits(UnitType type){
		return type.unit;
	}
}
