/**
 * Enum that contain unit of area.
 * @author Voraton Lertrattanapaisal
 *
 */
public enum Area implements Unit {
	M2("Square Meter",1),
	AC("Acre",4046.8564224 ),
	A("Are",100 ),
	B("Barn",1E-28 ),
	HA("Hectare",10000 ),
	HS("Homestead",647497.027584 ),
	R("Rood",1011.7141056 ),
	CM2("Square Centimeter",0.0001 ),
	FT2("Square Foot",0.09290304 ),
	IN2("Square Inch",0.00064516 ),
	KM2("Square Kilometer",1000000 ),
	MI2("Square Mile",2589988.110336 ),
	MM2("Square Millimeter",0.000001 ),
	R2("Square ROD",25.29285264),
	YR2("Square Yard",0.83612736 ),
	TWP("Township",93239571.9721 ),
	RAI("Rai",12141),
	WA2("Square Wa",1349),
	NY("Nyarn",134900);
	
	
	public final String name;
	public final double value;
	/**
	 * To initialize the Unit of Area.
	 * @param name is name of unit.
	 * @param value is base value of unit.
	 */
	private Area(String name,double value){
		this.name=name;
		this.value=value;
	}
	/**
	 * To get base value of unit.
	 */
	public double getValue(){
		return value;
	}
	/**
	 * To convert amount to other unit.
	 * @param amt is amount to convert.
	 * @param unit is other unit that you want to convert to.
	 */
	public double convertTo(double amt,Unit unit){
		return (amt*this.getValue())/(unit.getValue());
	}
	/**
	 * To return unit name.
	 */
	public String toString(){
		return name;
	}
}
