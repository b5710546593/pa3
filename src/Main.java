/**
 * Main for show Converter Window.
 * @author Voraton Lertrattanapaisal
 *
 */
public class Main {
	/** 
	 * For run main to show Converter Window.
	 * @param args is argument for can run String.
	 */
	public static void main(String[] args) {
		UnitConverter uc = new UnitConverter();
		ConverterUI ui = new ConverterUI(uc);
		ui.run();
	}

}
