/**
 * For Store Unit array.
 * @author Voraton Lertrattanapaisal
 *
 */
public enum UnitType {
	LengthUnit( Length.values()),
	AreaUnit( Area.values()),
	WeightUnit( Weight.values()),
	VolumeUnit(Volume.values());
	public Unit[] unit;
	/**
	 * Initialize the UnitType .
	 * @param unit is array of unit.
	 */
	private UnitType(Unit[] unit){
		this.unit=unit;
	}
}