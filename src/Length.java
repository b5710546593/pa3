/**
 * Enum that contain unit of length.
 * @author Voraton Lertrattanapaisal
 *
 */
public enum Length implements Unit{
	METER("Meter",1.0),
	KILOMETER("Kilometer",1000.0),
	MILE("Mile",1609.344),
	FOOT("Foot",0.30480),
	WA("Wa",2.0),
	CM("Centimeter",0.01),
	MM("Millimeter",0.001),
	YARD("Yard",0.9144),
	Inch("Inch",0.0254),
	LIGHT_YEAR("Light Year",9460660000000000.0),
	AU("Astronomical Unit",149597870691.0),
	DM("Decimeter",0.1),
	UM("Micrometer",0.000001),
	NM("Nanometer",1E-9),
	ANGSTORM("Angstorm",1E-10),
	CH("Chain",20.1168),
	FT("Fathom",1.8288 ),
	FL("Furlong",201.168 ),
	LG("League",4828.032 ),
	NMILE("Nautical Mile",1852.0 ),
	PS("Parsec",30856775813060000.0),
	ROD("Rod",5.0292 ),
	S("Sork",0.5),
	Sen("Sen",40),
	L("Lah",0.9144),
	K("Kueb",0.25),
	Y("Yosh",62.5);
	
	public final String name;
	public final double value;
	/**
	 * To initialize the Unit of Length.
	 * @param name is name of unit.
	 * @param value is base value of unit.
	 */
	private Length(String name,double value){
		this.name=name;
		this.value=value;
	}
	/**
	 * To get base value of unit.
	 */
	public double getValue(){
		return value;
	}
	/**
	 * To convert amount to other unit.
	 * @param amt is amount to convert.
	 * @param unit is other unit that you want to convert to.
	 */
	public double convertTo(double amt,Unit unit){
		return (amt*this.getValue())/(unit.getValue());
	}
	/**
	 * To return unit name.
	 */
	public String toString(){
		return name;
	}
}
